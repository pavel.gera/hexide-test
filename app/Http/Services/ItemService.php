<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\DB;

class ItemService
{
    public function getItems()
    {
        $items = DB::table('items')->paginate(15);

        return $items;
    }

    public function saveItem($data )
    {
        DB::table('items')->insert($data);
    }
}
