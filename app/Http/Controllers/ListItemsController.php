<?php

namespace App\Http\Controllers;

use App\Http\Services\ItemService;
use App\Models\User;
use App\Requests\AuthUserRequest;
use Illuminate\Http\Request;


class ListItemsController extends Controller
{
    public function __construct(protected ItemService $itemService)
    {}

    public function list(AuthUserRequest $request)
    {
        $data = $request->validated();
        if (!auth()->attempt($data)) {
            return response(['You are not allowed to list items']);
        }
        $items = $this->itemService->getItems();
        return response(['items' => $items], 200);
    }

    public function save(Request $request, User $user)
    {
        if ($request->user()->cannot('update', $user)) {
            return response()->json(null, 403);
        }
        $this->itemService->saveItem($request->toArray());
        return response()->json(null, 200);
    }
}
