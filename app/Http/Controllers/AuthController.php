<?php

namespace App\Http\Controllers;

use App\Requests\AuthUserRequest;

class AuthController extends Controller
{
    public function login(AuthUserRequest $authUserRequest)
    {
        $data = $authUserRequest->validated();
        if (!auth()->attempt($data)) {
            return response(['error_message' => 'Incorrect Details.
            Please try again']);
        }

        $token = auth()->user()->createToken('API Token')->accessToken;
        return response()->json(['token' => $token], 201);
    }

}
