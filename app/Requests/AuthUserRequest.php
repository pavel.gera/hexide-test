<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'email|required',
            'password' => 'required|confirmed',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
