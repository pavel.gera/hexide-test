<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/user', [App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::get('/items', [\App\Http\Controllers\ListItemsController::class, 'list'])->name('list');
Route::put('/save', [\App\Http\Controllers\ListItemsController::class, 'save'])->name('save');
